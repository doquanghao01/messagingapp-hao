import 'package:flutter/material.dart';
import 'package:messagingapp_hao/screens/signinOrSingUp/sigin_or_singup_screen.dart';

import '../../config/colors.dart';
import '../../widgets/text_bodyText1.dart';

class WelcomeScreen extends StatelessWidget {
  const WelcomeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            const Spacer(flex: 2),
            Image.asset("assets/images/welcome_image.png"),
            const Spacer(flex: 3),
            Text(
              "Welcome to our freedom \nmessaging app",
              textAlign: TextAlign.center,
              style: Theme.of(context)
                  .textTheme
                  .headline5!
                  .copyWith(fontWeight: FontWeight.bold),
            ),
            const Spacer(),
            const TextBodyText1(
              text: "Freedom talk any person of your \nmother language.",
              withOpaxity: 0.64,
            ),
            const Spacer(flex: 3),
            FittedBox(
              child: TextButton(
                onPressed: () => Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const SigninOrSignupScreen(),
                  ),
                ),
                child: Row(
                  children: [
                    const TextBodyText1(
                      text: "Skip",
                      withOpaxity: 0.8,
                    ),
                    const SizedBox(width: kDefaultPadding / 4),
                    Icon(
                      Icons.arrow_forward_ios,
                      size: 16,
                      color: Theme.of(context)
                          .textTheme
                          .bodyText1!
                          .color!
                          .withOpacity(0.8),
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
