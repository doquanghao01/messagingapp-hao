import 'package:flutter/material.dart';

class TextBodyText1 extends StatelessWidget {
  const TextBodyText1({
    Key? key,
    required this.text,
    required this.withOpaxity,
  }) : super(key: key);
  final String text;
  final double withOpaxity;
  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      textAlign: TextAlign.center,
      style: TextStyle(
        color: Theme.of(context)
            .textTheme
            .bodyText1!
            .color!
            .withOpacity(withOpaxity),
      ),
    );
  }
}
